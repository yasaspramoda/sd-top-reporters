# Top Reporters (Atlassian Add-on using Express - done as a homework for interview)

1. My working code is in **./public/js/addon2.js** (which is called in **./views/layout.hbs**)
2. A screencast demo is in the file **homework_demo-2019-02-18.mkv**
3. A second attempt with a more sophisticated solution that allows users to select the project from a dropdown is in the file **./public/js/addon.js**. Even though this is functioning and the page is rendering for a split second, a security error is thrown from the backend, which I couldn't solve in the given time. So I stuck with initial solution by hardcoding the project name.
