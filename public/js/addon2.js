const getBaseUrl = (urlString) => {
    if (urlString == null) {
        return '';
    }
    return urlString.substring(0, urlString.indexOf('.net') + 4);
}

const aggregateRequestsAndSort = (issues) => {
    const reporterMap = {};
    $.each(issues, (index, issue) => {
        // Filter only the requests: I wasn't sure about this. Do we show all types or just the "requests"
       if (issue.fields.issuetype.name !== 'Service Request') {
           return;
       } 
       // create a map of user name and number of requests created by the user
       if (reporterMap[issue.fields.creator.accountId] == null) {
           reporterMap[issue.fields.creator.accountId] = ({ 
               noOfRequests:1, 
               displayName: issue.fields.creator.displayName,
               url: issue.fields.creator.self
            });
       } else {
           reporterMap[issue.fields.creator.accountId].noOfRequests++;
       }
    });
    // create a list of reporter objects using the above map
    const reporters = $.map(reporterMap, (value, key) => (
        { 
            name: value.displayName, 
            noOfRequests: value.noOfRequests, 
            url: getBaseUrl(value.url) + `/people/${key}` 
        }));
    //sort by descending order of number of requests, so the most requestors come to the top
    reporters.sort((a, b) => 
        (a.noOfRequests < b.noOfRequests) ? 1 : ((a.noOfRequests > b.noOfRequests) ? -1 : 0)
    );

    return reporters;
} 


$(document).ready(() => {
    AP.require(['request', 'messages'], (request, messages) => {
        // Get all the service requests using JQL
        request({
            url: '/rest/api/3/search?jql=' + encodeURI('project="Test service desk"'), // Hardcoded the project name as I couldn't find any way of getting this.
            success: (response) => {
                const json = JSON.parse(response);
                if (json.issues.length > 0) {
                    const sortedReporters = aggregateRequestsAndSort(json.issues); //call the top reporters function
                    // show the results in a table (top reporters at the top)
                    $('<table>').addClass('aui').append(
                        $('<thead>').append(
                            $('<tr>').append(
                                $('<th>').text('Reporter'),
                                $('<th>').text('Number of requests')
                            )
                        ),
                        $('<tbody>').append(
                            $.map(sortedReporters, (reporter) => {
                                return $('<tr>').append(
                                    $('<td>').append(
                                        $('<a>').attr('href', reporter.url)
                                                .attr('target', '_top')
                                                .text(reporter.name)
                                    ),
                                    $('<td>').text(reporter.noOfRequests)
                                );
                            })
                        )
                    ).appendTo('#main-content');
                } else {
                    $('<div>').addClass('aui-message').append(
                        $('<p>').addClass('title').append(
                            $('<span>').addClass('aui-icon').addClass('icon-info'),
                            $('<strong>').text("Looks like there are no requests yet!")
                        )
                    ).appendTo('#main-content');
                }
            },
            error: (err) => {
                $('<div>').addClass('aui-message').addClass('aui-message-error').append(
                    $('<p>').addClass('title').append(
                        $('<span>').addClass('aui-icon').addClass('icon-error'),
                        $('<strong>').text('An error occurred!')
                    ),
                    $('<p>').text(err.status + ' ' + err.statusText)
                ).appendTo('#main-content');
            }
        });
    });
 });


