const getBaseUrl = (urlString) => {
    if (urlString == null) {
        return '';
    }
    return urlString.substring(0, urlString.indexOf('.net') + 4);
}

const aggregateRequestsAndSort = (issues) => {
    const reporterMap = {};
    $.each(issues, (index, issue) => {
        // Filter only the requests: I wasn't sure about this. Do we show all types or just the "requests"
       if (issue.fields.issuetype.name !== 'Service Request') {
           return;
       } 
       if (reporterMap[issue.fields.creator.accountId] == null) {
           reporterMap[issue.fields.creator.accountId] = ({ 
               noOfRequests:1, 
               displayName: issue.fields.creator.displayName,
               url: issue.fields.creator.self
            });
       } else {
           reporterMap[issue.fields.creator.accountId].noOfRequests++;
       }
    });
    
    const reporters = $.map(reporterMap, (value, key) => (
        { 
            name: value.displayName, 
            noOfRequests: value.noOfRequests, 
            url: getBaseUrl(value.url) + `/people/${key}` 
        }));
    //sort by descending order of number of requests, so the most requestors come to the top
    reporters.sort((a, b) => 
        (a.noOfRequests < b.noOfRequests) ? 1 : ((a.noOfRequests > b.noOfRequests) ? -1 : 0)
    );

    return reporters;
} 

// Show the top reporters 
const showTopReporters = (projectName) => {
    // Get all the service requests using JQL
    AP.require(['request', 'messages'], (request, messages) => {
        request({
            url: '/rest/api/3/search?jql=' + encodeURI(`project="${projectName}"`),
            headers:{ 
                'Authorization': 'JWT {{token}}',
                'Accept': 'application/json',
            },
            success: (response) => {
                const json = JSON.parse(response);

                if (json.issues.length > 0) {
                    const sortedReporters = aggregateRequestsAndSort(json.issues);

                    $('<table>').addClass('aui').append(
                        $('<thead>').append(
                            $('<tr>').append(
                                $('<th>').text('Reporter'),
                                $('<th>').text('Number of requests')
                            )
                        ),
                        $('<tbody>').append(
                            $.map(sortedReporters, (reporter) => {
                                return $('<tr>').append(
                                    $('<td>').append(
                                        $('<a>').attr('href', reporter.url)
                                                .attr('target', '_top')
                                                .text(reporter.name)
                                    ),
                                    $('<td>').text(reporter.noOfRequests)
                                );
                            })
                        )
                    ).appendTo('#main-content');
                } else {
                    $('<div>').addClass('aui-message').append(
                        $('<p>').addClass('title').append(
                            $('<span>').addClass('aui-icon').addClass('icon-info'),
                            $('<strong>').text("Looks like there are no requests yet!")
                        )
                    ).appendTo('#main-content');
                }
            },
            error: (err) => {
                $('<div>').addClass('aui-message').addClass('aui-message-error').append(
                    $('<p>').addClass('title').append(
                        $('<span>').addClass('aui-icon').addClass('icon-error'),
                        $('<strong>').text('An error occurred!')
                    ),
                    $('<p>').text(err.status + ' ' + err.statusText)
                ).appendTo('#main-content');
            }
        });
    });

    return;
}


$(document).ready(() => {
    // First get all the service desks available in the instance and show them to the user to select 
    AP.require(['request', 'messages'], (request, messages) => {
        request({
            url: '/rest/servicedeskapi/servicedesk',
            headers:{ 
                'Accept': 'application/json'
            },
            success: (response) => {
                const json = JSON.parse(response);

                $('<div>').addClass('aui').append(
                    $('<p>').addClass('title').append(
                        $('<strong>').text("Select the service desk project")
                    )
                ).appendTo('#main-content');
                
                $('<div>').addClass('aui').append(
                    $('<form id="sd-form">')
                        .addClass('aui')
                        .append(
                            $('<select id="sd-projects">').append(
                                $.map(json.values, (value) => {
                                    return $('<option>').attr('value', value.projectId).text(value.projectName);
                                })
                            ),
                            $('<input>').attr('type', 'submit').attr('value', 'Show Top Reporters')
                        )
                ).appendTo('#main-content');

                $('#sd-form').submit(() => {
                    const projectName = $('#sd-projects :selected').text();
                    showTopReporters(projectName);
                })
            },
            error: (err) => {
                $('<div>').addClass('aui-message').addClass('aui-message-error').append(
                    $('<p>').addClass('title').append(
                        $('<span>').addClass('aui-icon').addClass('icon-error'),
                        $('<strong>').text('An error occurred!')
                    ),
                    $('<p>').text(err.status + ' ' + err.statusText)
                ).appendTo('#main-content');
            }
        });
    });
});
